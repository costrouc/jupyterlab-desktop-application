#!/bin/bash

PYTHON=python3.6
PROJECT_PATH=$(dirname $(readlink -e $0))
JUPYTERLAB_PORT=8888
NOTEBOOK_DIRECTORY="/home/costrouc/p/personal/notebooks/"

install_jupyter() {
    echo "Installing: Jupyter Icon "
    mkdir -p $HOME/.icons/
    cp $PROJECT_PATH/jupyter.png $HOME/.icons/jupyter.png

    echo "Installing: Jupyter Desktop File"
    mkdir -p $HOME/.local/share/applications/
    cat <<EOF > $HOME/.local/share/applications/jupyter.desktop
[Desktop Entry]
Version=1.0
Type=Application
Icon=jupyter
Name=Jupyter
Exec=$PROJECT_PATH/launch.sh
EOF

    echo "Installing: Notebook Launcher Script"
    cat <<EOF > $PROJECT_PATH/launch.sh
#!/bin/bash
cd $PROJECT_PATH
VENV_PATH=$(pipenv --venv)
if [[ ! -d "\$VENV_PATH" ]]; then
   exit 1
fi
source \$VENV_PATH/bin/activate
jupyter lab --port=$JUPYTERLAB_PORT --port-retries=0 --notebook-dir=$NOTEBOOK_DIRECTORY
if [[ "\$?" != 0 ]]; then
    xdg-open "http://localhost:$JUPYTERLAB_PORT"
fi
EOF
    chmod +x $PROJECT_PATH/launch.sh
}

cd $PROJECT_PATH
pipenv install
install_jupyter
