# Linux notebook install
Requires: `pipenv`

Will install jupyter notebook `bash install.sh`

# Add virtualenv to jupyterlab
1. `pipenv install ipykernel`
2. `pipenv shell`
3. `ipython kernel install --user --name <venv name>`
